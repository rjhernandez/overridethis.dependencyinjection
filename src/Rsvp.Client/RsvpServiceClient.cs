﻿namespace Rsvp.Client.ServiceReference
{
    public partial class RsvpServiceClient
    {
        private static RsvpServiceClient _instance;
        public static RsvpServiceClient Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new RsvpServiceClient();
                return _instance;
            }
        }
    }
}
