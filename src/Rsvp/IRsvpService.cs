﻿using System.ServiceModel;
using Rsvp.Impl;

namespace Rsvp
{
    [ServiceContract]
    public interface IRsvpService
    {
        [OperationContract]
        Impl.Rsvp[] Rsvps();

        [OperationContract]
        RsvpResult Register(Impl.Rsvp request);

        [OperationContract]
        bool AlreadyRegistered(string email);
    }

}
