﻿using System.Data.Entity.Validation;
using System.Runtime.Serialization;

namespace Rsvp.Impl
{
    [DataContract(Name="RsvpResponse")]
    public class RsvpResult
    {
        [DataMember]
        public bool Success { get; set; }

        [DataMember]
        public string Message { get; set; }

        public static RsvpResult Successful()
        {
            return Successful("Registration Complete");
        }
        
        public static RsvpResult Successful(string message)
        {
            return Complete(true, message);
        }

        public static RsvpResult Complete(bool success, string message)
        {
            return new RsvpResult() { Success = success, Message = message };
        }

        public static RsvpResult Failure(DbEntityValidationException validationException)
        {
            var errorMsg = "";
            foreach (var entity in validationException.EntityValidationErrors)
            {
                foreach (var error in entity.ValidationErrors)
                {
                    errorMsg += string.Format("ERROR: {0}, {1}", error.PropertyName, error.ErrorMessage);
                }
            }
            return RsvpResult.Complete(false, errorMsg);
        }

        public static RsvpResult EmailNotUnique()
        {
            return Complete(false, "Email must be unique");
        }
    }
}
