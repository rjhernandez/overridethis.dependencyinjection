﻿using System.Data.Entity;
using System.Linq;

namespace Rsvp.Impl
{
    public class RsvpDataContext : DbContext
    {
        public RsvpDataContext() : base("RsvpDb")
        {
        }

        public DbSet<Rsvp> Rsvps { get; set; }

        public bool AlreadyRegistered(string email)
        {
            return this.Rsvps.Any(rsvp => rsvp.Email == email);
        }
    }
}
