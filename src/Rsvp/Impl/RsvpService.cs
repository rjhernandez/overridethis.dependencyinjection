﻿using System;
using System.Data.Entity.Validation;
using System.Linq;

namespace Rsvp.Impl
{
    public class RsvpService : IRsvpService
    {
        public Rsvp[] Rsvps()
        {
            using (var dataContext = new RsvpDataContext())
            {
                return dataContext.Rsvps.ToArray();
            }
        }

        public RsvpResult Register(Rsvp request)
        {
            try
            {
                using (var dataContext = new RsvpDataContext())
                {
                    if (dataContext.AlreadyRegistered(request.Email))
                        return RsvpResult.EmailNotUnique();

                    request.Created = DateTime.Now;
                    dataContext.Rsvps.Add(request);
                    dataContext.SaveChanges();
                    return RsvpResult.Successful();
                }
            }
            catch (DbEntityValidationException errors)
            {
                return RsvpResult.Failure(errors);
            }
        }

        public bool AlreadyRegistered(string email)
        {
            using (var dataContext = new RsvpDataContext())
            {
                return (dataContext.AlreadyRegistered(email));
            }
        }
    }
}
