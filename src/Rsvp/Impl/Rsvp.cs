﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Rsvp.Impl
{
	[DataContract(Name="RsvpRequest")]
	public class Rsvp
	{
		public Rsvp ()
		{
			this.Created = DateTime.Now;
		}

        [Key]
        public int Id { get; set; }

		[DataMember, Required]
		public string Firstname { get; set; }

		[DataMember, Required]
		public string Lastname { get; set; }

		[DataMember, Required]
		public string Email { get; set; }

		[DataMember, Required]
		public int NumberOfGuests { get; set; }

		public DateTime Created { get; set; }
	}
}
