﻿using System.Windows;
using Rsvp.Client.ServiceReference;

namespace Rsvp.WinClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Rsvp_Click(object sender, RoutedEventArgs e)
        {
            var svc = RsvpServiceClient.Instance;

            var request = new RsvpRequest()
                              {
                                  Firstname = this.Firstname.Text,
                                  Lastname = this.Lastname.Text,
                                  Email = this.Email.Text,
                                  NumberOfGuests = (int)this.NumberOfGuests.Value
                              };

            var response = svc.Register(request);
            this.Errors.Content = response.Message;
        }
    }
}
