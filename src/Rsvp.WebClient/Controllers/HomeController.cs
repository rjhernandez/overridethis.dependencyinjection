﻿using System.Web.Mvc;
using Rsvp.Client.ServiceReference;

namespace Rsvp.Web.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
            var svc = RsvpServiceClient.Instance;
            return View(svc.Rsvps());
        }

        public ActionResult Refresh()
        {
            return RedirectToAction("Index");
        }
    }
}
