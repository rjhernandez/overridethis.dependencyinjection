Introduction to Dependency Injection & IoC
=========

Demos and slides for presentation on Dependency Injection as it applies to building solutions on the Microsoft .NET platform.  The demos consist of a basic event attendee tracking application that has three components.

+ Services - a hosted WCF soap WebService that provides the ability to add attendees to an event.
+ Windows Client - a WPF client application that uses the WebService to add attendees to an event.
+ WebSite - and ASP.NET MVC4 website that uses the WebService to list an events attendee list.

*All Dependency Injection demos are written using Autofac (https://code.google.com/p/autofac/).
